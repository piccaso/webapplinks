Welcome to Webapp Links

If you have <meta name="apple-mobile-web-app-capable" content="yes">
enabled in your Theme, you need this module to prevent an iOS device
to open links (if the website was added to the homescreen)
to open in a new Mobile Safari App.

If you're having trouble installing this module, please ensure that your
tar program is not flattening the directory tree, truncating filenames
or losing files.

Installing Webapp Links Module:

Place the entirety of this directory in sites/all/modules/webapplinks
 
Navigate to administer >> build >> modules. Enable Webapp Links.

